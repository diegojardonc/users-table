# UsersTable

# Schema

```javascript
class UsersTable extends Component {
    render () {
        // UsersTableHead manages thead and th render: receives fields from UsersTable (users' first element's keys are the default)
        <UsersTableHead fields={this.props.fields || Object.keys(this.props.users[0])}>
        // UsersTableBody manages tbody, tr, td render: receives users from UsersTableHead and required fields 
        <UsersTableBody users={this.props.users} fields={this.props.fields || Object.keys(this.props.[0])}/>
    }
}
```

## `props`

`users` (required): An array of objects (`user`) containing `fields` as keys and strictly containing `string` objects as values (except for id key `number`).  
                    By default only first member of the array is used for validation of fields passed as props (this assumes all user objects contain keys requested in fields).
        
`fields`: An array of `string` objects containing the requested keys from `user` objects. (It has to match `user` object keys).
         `id` element of this `prop` is ignored so that `user.id` is only used for keying list rendering so that it's not rendered. 

`nostrict`: `boolean` value, by default `false`, if true, it skips validation of `user` object keys matching `fields` requested in props.  
            Might draw in render problems.

## Usage

```html
<script>
const users = [
    {
        id: 0,
        name: 'John Doe',
        username: 'johndoe11',
        email: 'johndoe11@email.com',
        address: 'Holy Bible Av. Apt 201 70001'
    },
    {
        id: 1,
        name: 'Chris Green',
        username: 'chrisgreen22',
        email: 'chrisgreen22@email.com',
        address: 'Great Green Av. Apt 101 70021'
    }
];

const fields = ['name','username', 'email'];

</script>    

    /*  Creates a table with user object keys as fields for thead
        and maps all objects values in tbody */
    <UsersTable users={users}/>
    
    /*  Renders a table with only fields requested from user object keys
        by default first key is assumed as id and skipped for keying usage only*/
    <UsersTable users={users} fields={fields}/>
```
### How do I "normalize" my objects to contain just string
 
If your `user` objects contain objects so that they don't render in the component correctly
you can use `normalizeObject(object, character)` function exported in `utilities.js` module to stringify object's
object keys to strings containing its values joint by the character specified as second argument, if character === undefined
the object's values are joint by an empty string ''.

```javascript
const object = {
  object : {
    key: 'value'
    key2: 'value2'
  }
}

console.log(normalizeObject(object, ' ');
// Returns a copy normalized copy of the object passed as first argumen
// Object { object: 'value value2'

console.log(normalizeObject(object, /*false or undefined*/))
// Object { object: "valuevalue2" }
```
## Build and run

```
    git clone https://gitlab.com/diegojardonc/users-table.git
    cd users-table
    npm install
    npm run build
    serve -s build
```


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).