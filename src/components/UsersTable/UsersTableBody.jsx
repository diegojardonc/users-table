import React, { Component } from 'react';
import PropTypes from 'prop-types';
import UsersTableBodyRow from './UsersTableBodyRow';


export default class UsersTableBody extends Component {
    static propTypes = {
        users: PropTypes.array.isRequired,
        fields: PropTypes.array
    };

    render () {

        const rows = this.props.users.map((user) => 
            <UsersTableBodyRow key={user.id} user={user} fields={this.props.fields || Object.keys(user)}/>
        );

        return (
            <tbody className="users-table__body">
                {rows}
            </tbody>
        )
    }
};
