import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UsersTableHead extends Component {

  static propTypes = {
    fields: PropTypes.array.isRequired
  }

  render () {

    const fields = this.props.fields.map((field, index) => 
      <th className="users-table__head__row__field" key={index}>{field}</th>
    );

    return (
      <thead className="users-table__head">
        <tr className="users-table__head__row">
          {fields}
        </tr>
      </thead>
    );
  }

};
