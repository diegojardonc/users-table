import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UsersTableBodyRow extends Component {

    static propTypes = {
        user: PropTypes.object.isRequired
    }

    isDomain(string) {
        return  /^((([0-9]{1,3}\.){3}[0-9]{1,3})|(([a-zA-Z0-9]+(([\-]?[a-zA-Z0-9]+)*\.)+)*[a-zA-Z]{2,}))$/.test(string);
    }

    render () {

        const requestedFields = this.props.fields.map((field) => 
            this.props.user[field]
        );

         const fieldsSource = requestedFields || Object.values(this.props.user); 


        const tableDataFields = fieldsSource.map((field, index) => {
                if (this.isDomain(field) && this.props.user['website'] === field) {
                    return <td className="users-table__body__row__field" key={index}><a className="users-table__body__row__field__website" href={`https://${field}/`}>{field}</a></td>
                } else if (index === 0 && (this.props.fields === undefined)) {
                    return  null;
                } else if (this.props.user['username'] === field) {
                    return <td className="users-table__body__row__field" key={index} style={{'display': 'flex', 'align-items': 'center'}}><img src="/user.png" alt="" className="users-table__body__row__field__image"/>{field}</td>
                } 
                
                return <td className="users-table__body__row__field" key={index}>{field}</td>;
            });

        return (
            <tr className="users-table__body__row">
                {tableDataFields}
            </tr>
        );
    }
};
