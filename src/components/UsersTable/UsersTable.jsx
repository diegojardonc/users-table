import React, { Component } from 'react';
import PropTypes from 'prop-types';
import UsersTableHead from './UsersTableHead';
import UsersTableBody from './UsersTableBody';
import './UsersTable.css';

export default class UsersTable extends Component {

    static propTypes = {
        users: PropTypes.array.isRequired,
        fields: PropTypes.array
    }
    
    render () {
        const noId = Object.keys(this.props.users[0]);
        this.fields = this.props.fields || noId;
        let fieldsInUsers = Object.keys(this.props.users[0]).includes(this.fields);

            // This verification assumes all array elements (user) of users array are equal
            if (!(this.fields.length === Object.keys(this.props.users[0]).length) && (!fieldsInUsers && !(this.fieldsInUsers === undefined)) && !this.props.nostrict) {
                throw new Error("Fields requested to render do not match with users data keys");
            }

            if (!(this.fields.indexOf('id') === -1)) {
                this.fields.shift();
            }

        return (
            <table className="users-table">
                <UsersTableHead fields={this.fields}/>
                <UsersTableBody users={this.props.users} fields={this.fields}/>
            </table>
        );
    }
};
