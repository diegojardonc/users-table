export function hasObjectChildren (object) {
    const keys = Object.keys(object); 
    let objectChildrenCounter = 0;

    for (let key of keys) {
        if (typeof object[key] === "object") {
            objectChildrenCounter++;
        }
    }
    
    return objectChildrenCounter;
};

export function normalizeObject (object, char) {

    if(char === undefined) {
        char = '';
    }
    const keys = Object.keys(object);

    for (let key of keys) {
      if (typeof object[key] === "object") {
        let subkeys = Object.keys(object[key])
        for (let subkey of subkeys) {
          if (typeof object[key][subkey] === "object") {
            object[key][subkey] = Object.values(object[key][subkey]).join(char);
          }
        }
        object[key] = Object.values(object[key]).join(char);
      }
    }

    return object;
  }